# Trabajos Teóricos
## Sistemas de Soporte para la Toma de Decisiones

#

### Los trabajos teóricos realizados se encuentran en las siguientes carpetas:

* [Trabajo Teórico N°1](./TT1)
  * [Archivo](./TT1/DSS2020-TT1-Bianchini.pdf)
* [Trabajo Teórico N°2](./TT2)
  * [Archivo](./TT2/DSS2020-TT2-Bianchini.pdf)
* [Trabajo Teórico N°3](./TT3)
  * [Archivo](./TT3/DSS2020-TT3-Bianchini.pdf)