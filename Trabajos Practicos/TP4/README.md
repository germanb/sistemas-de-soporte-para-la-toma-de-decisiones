# Trabajo Práctico 4

## Enunciado
El enunciado completo del trabajo se encuentra [aquí](./DSS2020-TP4-Bianchini.pdf)

## Stack docker
El [stack docker](./docker-compose.yml) del trabajo práctico 4 cuenta con los siguientes servicios:
- etl: Sistema heredado del [Trabajo Práctico N°3](../TP3) para modelar el DW con los datos de casos de Covid-19 en Argentina
- jupyter: Expuesto en el puerto 8888 y utilizado para resolver todos los puntos del trabajo.
- mongodb: Utilizado para el punto 2 de la sección de Minería de Datos.
- postgres: Utilizado para almacenar el DW de datos de casos de Covid-19 en Argentina.

**NOTA: Se hereda el servicio de python (Con el sistema ETL desarrollado) del trabajo anterior para utilizar los datos en la sección de visualización de datos** 

## Ejecución del stack:
1. Dentro del directorio [Servicios](./Servicios) se encuentran los directorios que contienen los archivos de interés para cada servicio. Dentro de ellos, se encuentran archivos de ambiente .env.template ([dw](./Servicios/dw/.env.template), [etl](./Servicios/etl/.env.template), [jupyter](./Servicios/jupyter/.env.template) y [mongo](./Servicios/mongo/.env.template)) que contienen variables necesarias para la ejecución del stack. Se debe ingresar a cada uno de esos directorios para generar un .env para cada servicio con las variables correspondientes.
2. Una vez generados todos los archivos .env, ejecutar el comando ```docker-compose up -d``` en el mismo directorio del archivo ```docker-compose.yml``` para levantar el stack.
3. Con el navegador, ir a la dirección ```localhost:8888``` (Contraseña: ```dss``` si se utiliza el .env por defecto) para entrar al notebook
4. Una vez dentro del notebook se visualizará la siguiente jerarquía de carpetas:
    - [Minería de Datos](./Resoluciones/Mineria\ de\ Datos)
        - [Punto 1](./Resoluciones/Mineria\ de\ Datos/Punto1)
            - [Punto 1.ipynb](./Resoluciones/Mineria\ de\ Datos/Punto1/Punto\ 1.ipynb): Notebook que contiene la resolución del punto 1
        - [Punto 2](./Resoluciones/Mineria\ de\ Datos/Punto2)
            - [punto2](./Resoluciones/Mineria\ de\ Datos/Punto2/punto2)
                - [resultados.json](./Resoluciones/Mineria\ de\ Datos/Punto2/punto2/resultados.json): Resultados del crawler en formato json.
                - [punto2](./Resoluciones/Mineria\ de\ Datos/Punto2/punto2/punto2)
                    - [spiders](./Resoluciones/Mineria\ de\ Datos/Punto2/punto2/spiders)
                        - [diario_jornada.py](./Resoluciones/Mineria\ de\ Datos/Punto2/punto2/spiders/diario_jornada.py): Spider utilizada para crawlear el sitio del diario jornada y extraer los datos.
                    - Demás archivos de scrapy...
            - [frecuencias_palabras.json](./Resoluciones/Mineria\ de\ Datos/Punto2/frecuencias_palabras.json): Dump de las frecuencias de las palabras encontradas en las noticias scrapeadas del diario jornada en formato json.
            - [Punto 2.ipynb](./Resoluciones/Mineria\ de\ Datos/Punto2/Punto\ 2.ipynb): Notebook que contiene la resolución del punto 2
        - [Punto 3](./Resoluciones/Mineria\ de\ Datos/Punto3)
            - [Punto 3.ipynb](./Resoluciones/Mineria\ de\ Datos/Punto3/Punto\ 3.ipynb): Notebook que contiene la resolución del punto 3
    - [Visualizacion de Datos](./Resoluciones/Visualizacion\ de\ Datos)
        - [Visualizacion Activa-Pasiva.ipynb](./Resoluciones/Visualizacion\ de\ Datos/Visualizacion\ Activa-Pasiva.ipynb): Resolucion completa de los puntos de visualización activa y pasiva.
        - [casos_por_provincia.html](./Resoluciones/Visualizacion\ de\ Datos/casos_por_provincia.html): html de salida de la herramienta bokeh con el gráfico interactivo.

## Sección: [Mineria de Datos](./Resoluciones/Mineria\ de\ Datos)
Para visualizar las soluciones de los ejercicios esta seccion ir a:
- [Resolución Punto 1](./Resoluciones/Mineria\ de\ Datos/Punto1/Punto\ 1.ipynb)
- [Resolución Punto 2](./Resoluciones/Mineria\ de\ Datos/Punto2/Punto\ 2.ipynb)
- [Resolución Punto 3](./Resoluciones/Mineria\ de\ Datos/Punto3/Punto\ 3.ipynb)

## Sección: [Visualización de Datos](./Resoluciones/Visualización\ de\ Datos)
Para visualizar las soluciones de los ejercicios esta seccion  ir a:
- [Resolución Punto 1](./Resoluciones/Visualizacion\ de\ Datos/Visualizacion\ Activa-Pasiva.ipynb)
