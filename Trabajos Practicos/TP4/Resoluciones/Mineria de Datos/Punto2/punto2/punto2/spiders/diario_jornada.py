import scrapy


class DiarioJornadaSpider(scrapy.Spider):
    name = 'diario-jornada'

    def start_requests(self):
        urls = [
            'https://www.diariojornada.com.ar/provincia/',
            'https://www.diariojornada.com.ar/policiales/',
            'https://www.diariojornada.com.ar/sociedad/',
            'https://www.diariojornada.com.ar/deportes/',
            'https://www.diariojornada.com.ar/paismundo/',
            'https://www.diariojornada.com.ar/economia/',
            'https://www.diariojornada.com.ar/espectaculos/',
            'https://www.diariojornada.com.ar/ciencia/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse_noticias(self, response):
        yield {
            "seccion": response.css("a[id=ContentPlaceHolder1_hl_Seccion]::text").get(),
            "titulo": response.css("span[id=ContentPlaceHolder1_lbl_Titulo]::text").get(),
            "texto": response.css("div[id=cuerpo] p::text").getall(),
            "visitas": int(response.css("span[id=ContentPlaceHolder1_lbl_Hits]::text").get().replace(".", ""))
        }
            
    def parse(self, response):
        urls_noticias = response.css("div[id*=_Titulo_] a::attr(href)").getall() + response.css("a[id*=_Titulo_]::attr(href)").getall()
        
        for url in urls_noticias:
            yield scrapy.Request(url=url, callback=self.parse_noticias)
