#!/bin/bash 

until pg_isready -h postgres -p 5432 -U dss_user
do
    echo "Esperando a que levante el contenedor de postgres..."
    sleep 5
done
sleep 2

python /etl/main.py

tail -f /dev/null