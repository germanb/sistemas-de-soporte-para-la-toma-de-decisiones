import sys
from sqlalchemy import create_engine, exc, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from .Extract import Extract
from .Transform import Transform
from .Load import Load
sys.path.append("..")
from utils.Constantes import TIME_TABLE_NAME, \
    PROV_TABLE_NAME, \
    TEST_TABLE_NAME, \
    COVID_TABLE_NAME, \
    PROCESSED_FILE_TABLE_NAME
from modelo.ProcessedFile import ProcessedFile
Base = declarative_base()


class ETL:
    def __init__(self, covid_file_name, provs_file_name):
        self.conn, self.engine, self.session = self.initialize_database()
        self.covid_file_name = covid_file_name
        self.provs_file_name = provs_file_name
        self.extract = Extract()
        self.transform = Transform(self.session, self.conn)
        self.load = Load(self.conn)

    def initialize_database(self):
        try:
            engine = create_engine('postgresql://dss_user:dss_passwd@postgres')
            with engine.connect() as connection:
                connection.execute('COMMIT')
                self.create_database(connection)
        except exc.OperationalError:
            raise Exception("No se pudo crear la conexion a la base de datos.")

        try:
            engine = create_engine('postgresql://dss_user:dss_passwd@postgres/p4')
            conn = engine.connect()
            conn.execute('COMMIT')

            self.create_tables(conn)

            Session = sessionmaker(bind=engine)

            return conn, engine, Session()
        except exc.OperationalError:
            raise Exception("No se pudo crear la base de datos.")

    def create_database(self, conn):
        try:
            print("Creando base de datos 'p4'")
            conn.execute('create database p4')
        except exc.ProgrammingError:
            print("> La base de datos 'p4' ya existe")

    def create_tables(self, conn):
        self.createTimeDimension(conn)
        self.createProvDimension(conn)
        self.createTestDimension(conn)
        self.createFactCovid(conn)
        self.createProcessedFile(conn)

    def createTimeDimension(self, conn):
        print("Creando tabla "+TIME_TABLE_NAME)
        try:
            conn.execute("""create table """+TIME_TABLE_NAME+"""(
                id serial primary key,
                dia int not null check(dia > 0 and dia <= 31),
                mes int not null check(mes > 0 and mes <= 12),
                año int
            )""")
        except exc.ProgrammingError:
            print("> Table "+TIME_TABLE_NAME+" ya existe")

    def createProvDimension(self, conn):
        print("Creando tabla "+PROV_TABLE_NAME)
        try: 
            conn.execute("""create table """+PROV_TABLE_NAME+"""(
                id serial primary key,
                codigo varchar not null,
                nombre varchar not null,
                densidad_poblacional varchar not null,
                poblacion_total varchar not null,
                porcentaje_pais varchar not null
            )""")
        except exc.ProgrammingError:
            print("> La tabla "+PROV_TABLE_NAME+" ya existe")

    def createTestDimension(self, conn):
        print("Creando tabla "+TEST_TABLE_NAME)
        try:
            conn.execute("""create table """+TEST_TABLE_NAME+"""(
                id serial primary key,
                negativos int not null,
                totales int not null
            )""")
        except exc.ProgrammingError:
            print("> La tabla "+TEST_TABLE_NAME+" ya existe")

    def createFactCovid(self, conn):
        print("Creando tabla "+COVID_TABLE_NAME)
        try:
            conn.execute("""create table """+COVID_TABLE_NAME+"""(
                id serial primary key,
                time_id int not null,
                test_id int not null,
                prov_id int not null,
                dia_inicio int,
                total_casos_confirmados int,
                total_casos_fallecidos int,
                total_casos_recuperados int,
                total_casos_terapia int,
                transmision_tipo varchar,
                constraint fk_time foreign key (time_id) references td_time(id),
                constraint fk_test foreign key (test_id) references td_test(id),
                constraint fk_prov foreign key (prov_id) references td_prov(id)
            )""")
        except exc.ProgrammingError:
            print("> La tabla "+COVID_TABLE_NAME+" ya existe")

    def createProcessedFile(self, conn):
        print("Creando tabla "+PROCESSED_FILE_TABLE_NAME)
        try:
            conn.execute("""create table """+PROCESSED_FILE_TABLE_NAME+"""(
                id serial primary key,
                nombre varchar,
                hash varchar
            )""")
        except exc.ProgrammingError:
            print("> Table "+PROCESSED_FILE_TABLE_NAME+" ya existe")

    def files_are_processed(self):
        covid_file_processed = self.session.query(ProcessedFile).filter_by(nombre=self.covid_file_name, hash=ProcessedFile.calculate_hash(self.covid_file_name)).count()
        provs_file_processed = self.session.query(ProcessedFile).filter_by(nombre=self.provs_file_name, hash=ProcessedFile.calculate_hash(self.provs_file_name)).count()
        return (covid_file_processed != 0 or provs_file_processed != 0)

    def save_processed_files(self):
        covid_file = ProcessedFile(nombre=self.covid_file_name, hash=ProcessedFile.calculate_hash(self.covid_file_name))
        provs_file = ProcessedFile(nombre=self.provs_file_name, hash=ProcessedFile.calculate_hash(self.provs_file_name))
        self.session.add(covid_file)
        self.session.add(provs_file)
        self.session.commit()

    def dump_resource(self, resource):
        pass  # Dumpear al archivo de logs

    def dump_database(self, database):
        pass  # Dumpear la base al archivo de logs

    def execute(self):
        if (not self.files_are_processed()):
            parsed_covid_file, parsed_provs_file = self.extract.execute(self.covid_file_name, self.provs_file_name)
            transformed_data = self.transform.execute(parsed_covid_file, parsed_provs_file)
            self.load.execute(self.session, transformed_data)
            self.save_processed_files()
        else:
            print("Los archivos indicados ya fueron procesados")
        self.session.commit()
        self.session.close()
        self.conn.execute('COMMIT')
        self.conn.close()
        self.engine.dispose()
