import re
import sys
import pandas as pd
sys.path.append("..")
from modelo.Tiempo import Tiempo
from modelo.Provincia import Provincia
from modelo.Test import Test
from modelo.Covid import Covid
from utils.Constantes import COLUMNAS_COVID, EQUIVALENCIAS_PROVINCIAS, COVID_TABLE_NAME, TIME_TABLE_NAME, PROV_TABLE_NAME, TEST_TABLE_NAME


class Transform:
    def __init__(self, session, conn):
        self.session = session
        self.conn = conn
        self.normalized_covid_data = None
        self.normalized_provs_data = None

    def normalize_covid_data(self, covid_data):
        for column_name, column_specs in COLUMNAS_COVID.items():
            covid_data.loc[covid_data[column_name].isnull(), column_name] = column_specs['valor_default']
            if column_name == 'fecha':
                covid_data[column_name] = pd.to_datetime(covid_data[column_name], format='%d/%m/%Y')
            else:
                covid_data[column_name] = (covid_data[column_name]).astype(column_specs['tipo'])
        self.normalized_covid_data = covid_data

    def normalize_provs_data(self, provs_data):
        self.normalized_provs_data = provs_data

    def buscar_id_provincia(self, nombre, provincias):
        try:
            prov = next(EQUIVALENCIAS_PROVINCIAS[key] for key in EQUIVALENCIAS_PROVINCIAS.keys() if re.match(key, nombre, re.IGNORECASE))
        except Exception:
            prov = next(EQUIVALENCIAS_PROVINCIAS[key] for key in EQUIVALENCIAS_PROVINCIAS.keys() if re.match(key, "indeterminado", re.IGNORECASE))

        return next(provincia['indice'] for provincia in provincias if provincia['provincia'].nombre == prov)

    def get_prov_table_data(self):
        prov_list = [{
                'indice': indice, 
                'provincia': Provincia(
                    codigo=valor['code'],
                    nombre=valor['name'],
                    densidad_poblacional=valor['pop_dens'],
                    poblacion_total=valor['total_pop'],
                    porcentaje_pais=valor['country_percentage'])
            } for indice, valor in enumerate(self.normalized_provs_data, start=1)
        ]
        prov_list.append({
            'indice': (len(prov_list)+1),
            'provincia': Provincia(
                codigo="INDE",
                nombre="Indeterminado",
                densidad_poblacional=0,
                poblacion_total=0,
                porcentaje_pais=0
            )
        })
        return prov_list

    def get_time_table_data(self):
        return [{
                'indice': indice, 
                'tiempo': Tiempo(
                    dia=fecha.day,
                    mes=fecha.month,
                    año=fecha.year
                )
            } for indice, fecha in enumerate(self.normalized_covid_data['fecha'].tolist(), start=1)
        ]

    def get_test_table_data(self):
        return [{
                'indice': indice, 
                'test': Test(
                    negativos=valor[0],
                    totales=valor[1]
                )
            } for indice, valor in enumerate(self.normalized_covid_data[['test_RT-PCR_negativos', 'test_RT-PCR_total']].values.tolist(), start=1)
        ]

    def get_covid_table_data(self, prov_data, time_data, test_data):
        return [{
                'indice': indice, 
                'covid': Covid(
                    dia_inicio=valor[0],
                    time_id=indice,
                    test_id=indice,
                    prov_id=self.buscar_id_provincia(valor[1], prov_data),
                    total_casos_confirmados=valor[2], 
                    total_casos_fallecidos=valor[3], 
                    total_casos_recuperados=valor[4], 
                    total_casos_terapia=valor[5], 
                    transmision_tipo=valor[6])
            } for indice, valor in enumerate(self.normalized_covid_data[['dia_inicio', 'osm_admin_level_4', 'tot_casosconf', 'tot_fallecidos', 'tot_recuperados', 'tot_terapia', 'transmision_tipo']].values.tolist(), start=1)
        ]

    def transform(self):
        prov_data = self.get_prov_table_data()
        time_data = self.get_time_table_data()
        test_data = self.get_test_table_data()
        covid_data = self.get_covid_table_data(prov_data, time_data, test_data)

        return {
            PROV_TABLE_NAME: prov_data,
            TIME_TABLE_NAME: time_data,
            TEST_TABLE_NAME: test_data,
            COVID_TABLE_NAME: covid_data
        }

    def execute(self, covid_data, provs_data):
        self.normalize_covid_data(covid_data)
        self.normalize_provs_data(provs_data)
        return self.transform()
