from sqlalchemy import Column, Integer, String
import sys
sys.path.append("..")
from utils.Constantes import Base, PROV_TABLE_NAME


class Provincia(Base):
    __tablename__ = PROV_TABLE_NAME

    id = Column(Integer, primary_key=True)
    codigo = Column(String)
    nombre = Column(String)
    densidad_poblacional = Column(Integer)
    poblacion_total = Column(Integer)
    porcentaje_pais = Column(Integer)
