from sqlalchemy import Column, Integer
import sys
sys.path.append("..")
from utils.Constantes import Base, TEST_TABLE_NAME


class Test(Base):
    __tablename__ = TEST_TABLE_NAME

    id = Column(Integer, primary_key=True)
    negativos = Column(Integer)
    totales = Column(Integer)
