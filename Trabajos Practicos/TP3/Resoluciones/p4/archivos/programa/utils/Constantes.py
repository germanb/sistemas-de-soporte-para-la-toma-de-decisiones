import numpy as np
from sqlalchemy.ext.declarative import declarative_base

DATABASE_NAME = 'p4'
DATABASE_HOST = 'bdd'
DATABASE_USER = 'dss_user'
DATABASE_PASSWD = 'dss_passwd'

COVID_FILE_NAME = "/p4/Recursos/covid-arg/export.csv"
PROVS_FILE_NAME = "/p4/Recursos/pop_provs.json"

TIME_TABLE_NAME = 'td_time'
PROV_TABLE_NAME = 'td_prov'
TEST_TABLE_NAME = 'td_test'
COVID_TABLE_NAME = 'th_covid'
PROCESSED_FILE_TABLE_NAME = 'processed_file'

COLUMNAS_COVID = {
    'fecha': {
        'valor_default': '01/01/1970',
        'tipo': np.dtype(np.datetime64)
    },
    'dia_inicio': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'osm_admin_level_4': {
        'valor_default': 'Indeterminado',
        'tipo': np.dtype(str)
    },
    'tot_casosconf': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'tot_fallecidos': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'tot_recuperados': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'tot_terapia': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'test_RT-PCR_negativos': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'test_RT-PCR_total': {
        'valor_default': -1,
        'tipo': np.dtype(int)
    },
    'transmision_tipo': {
        'valor_default': 'Indeterminado',
        'tipo': np.dtype(str)
    },
}
EQUIVALENCIAS_PROVINCIAS = {
    "caba": "Provincia de Buenos Aires",
    "buenos aires": "Ciudad Autónoma de Buenos Aires",
    "salta": "Provincia de Salta",
    "c[ó|o]rdoba": "Provincia de Córdoba",
    "r[íi]o Negro": "Provincia de Río Negro",
    "chubut": "Provincia del Chubut",
    "santa cruz": "Provincia de Santa Cruz",
    "catamarca": "Provincia de Catamarca",
    "santa fe": "Provincia de Santa Fe",
    "santiago del estero": "Provincia de Santiago del Estero",
    "la pampa": "Provincia de La Pampa",
    "mendoza": "Provincia de Mendoza",
    "chaco": "Provincia del Chaco",
    "nequ[eé]n": "Provincia del Neuquén",
    "la rioja": "Provincia de La Rioja",
    "san juan": "Provincia de San Juan",
    "corrientes": "Provincia de Corrientes",
    "formosa": "Provincia de Formosa",
    "entre r[íi]os": "Provincia de Entre Ríos",
    "san luis": "Provincia de San Luis",
    "jujuy": "Provincia de Jujuy",
    "misiones": "Provincia de Misiones",
    "tucum[aá]n": "Provincia de Tucumán",
    "tierra del fuego": "Provincia de Tierra del Fuego",
    "indeterminado": "Indeterminado"
}

Base = declarative_base()