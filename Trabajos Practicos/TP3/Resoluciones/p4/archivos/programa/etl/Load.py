import sys
sys.path.append("..")
from utils.Constantes import COVID_TABLE_NAME, TIME_TABLE_NAME, PROV_TABLE_NAME, TEST_TABLE_NAME


class Load:
    def __init__(self, conn):
        self.conn = conn

    def insert_time_data(self, session, data):
        inserts = session.bulk_save_objects([d.get('tiempo') for d in data])
        session.commit()

    def insert_prov_data(self, session, data):
        inserts = session.bulk_save_objects([d.get('provincia') for d in data])
        session.commit()

    def insert_test_data(self, session, data):
        inserts = session.bulk_save_objects([d.get('test') for d in data])
        session.commit()

    def insert_covid_data(self, session, data):
        inserts = session.bulk_save_objects([d.get('covid') for d in data])
        session.commit()

    def execute(self, session, transformed_data):
        time_inserts = self.insert_time_data(session, transformed_data[TIME_TABLE_NAME])
        prov_inserts = self.insert_prov_data(session, transformed_data[PROV_TABLE_NAME])
        test_inserts = self.insert_test_data(session, transformed_data[TEST_TABLE_NAME])
        covid_inserts = self.insert_covid_data(session, transformed_data[COVID_TABLE_NAME])
