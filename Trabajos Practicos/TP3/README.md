# Trabajo Práctico 3

## Para levantar el punto 4
1. Ejecutar el comando ```docker-compose up -d``` en el mismo directorio del archivo ```docker-compose.yml```
2. Ejecutar el comando ```docker exec python_dss python /p4/programa/main.py```


## TODO list / Bug list
- Logueo de prints al archivo ```ejecucion_<timestamp>.log```
- Falta el dump de la informacion de cada paso a un archivo separado de cada paso e identificado por un timestamp (```extract_<timestamp>.log```, ```transform_<timestamp>.log```, ```load_<timestamp>.log```)
- Tal vez meter los logs en una carpeta creada con el timestamp en lugar de poner el timestamp en los nombres de los archivos?
- La conexion a la base de datos (No se cierra correctamente con la terminación del programa)
- La persistencia de datos de pgadmin en el volumen no esta funcionando
- Guardado y commit de datos en pasos para poder valerse del ultimo id insertado para ejecuciones consecutivas de diferentes archivos (Los enumerate tienen hardcode el start en 1)
