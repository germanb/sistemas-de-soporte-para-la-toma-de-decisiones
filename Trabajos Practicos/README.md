# Trabajos Prácticos
## Sistemas de Soporte para la Toma de Decisiones

#

### Los trabajos prácticos realizados se encuentran en las siguientes carpetas:

* [Trabajo Práctico N°1](./TP1)
  * [Informe](./TP1/DSS2020-TP1-Bianchini.pdf)
* [Trabajo Práctico N°2](./TP2)
  * [Informe](./TP2/DSS2020-TP2-Bianchini.pdf)
* [Trabajo Práctico N°3](./TP3)
  * [Informe](./TP3/DSS2020-TP3-Bianchini.pdf)
* [Trabajo Práctico N°4](./TP4)
  * [Informe](./TP4/DSS2020-TP4-Bianchini.pdf)