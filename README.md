# Universidad Nacional de la Patagonia San Juan Bosco
## Asignatura: Sistemas de Soporte para la Toma de Decisiones (5to Año)
## Sede Trelew

En el presente repositorio se pueden encontrar las resoluciones a todos los trabajos prácticos de la materia Sistemas de Soporte para la Toma de Decisiones.

### Directorios
* [Trabajos Teóricos](./Trabajos\ Teoricos)
* [Trabajos Prácticos](./Trabajos\ Practicos)